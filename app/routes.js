(function () {

    'use strict';

    var app = require('express')();
    var cors = require('cors');

    var controllers = require('./controllers');
    var parametersService = require('./services/URLParametersService');
    var requests = require('./requests');

    module.exports = app;

    /**
     * To allow API calls from restricted domains
     */
    var whitelist = [
        'http://localhost'
    ];

    var corsOptions = {
        origin: function (origin, callback) {
            var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
            callback(null, originIsWhitelisted);
        },
        preflightContinue: false,
        credentials: true
    };
    app.use(cors(corsOptions));

    /**
     * Routing Parameters
     * Common variables used in the URL
     */
    app.param('hash', parametersService.validateHash);
    app.param('id', parametersService.validateId);

    /**
     * Welcome to the Node-TT API
     * and general open routes
     */
    app.get('/', controllers.App.getIndex);
    app.get('/api', controllers.App.getIndex);
    app.get('/api/health', controllers.App.getHealth);

    /*==============================
     =           VIEWS             =
     ==============================*/

    app.get('/page/test', controllers.Views.getTestPage);

    /*==============================
     =    REST POSTS               =
     ==============================*/

    app.post('/api/post', requests.AddPostRequest.validateRequest, controllers.Posts.addPost);
    app.put('/api/post', requests.EditPostRequest.validateRequest, controllers.Posts.editPost);
    app.delete('/api/post', requests.DeletePostRequest.validateRequest, controllers.Posts.deletePost);
    app.get('/api/posts', requests.GetPostsRequest.validateRequest, controllers.Posts.getPosts);
    app.get('/api/post', requests.GetPostRequest.validateRequest, controllers.Posts.findPost);

    /*==============================
     =            ERRORS            =
     ==============================*/

    app.use(controllers.Errors.errorNotFound);
    app.use(controllers.Errors.errorCatchAll);

})();