(function () {

    'use strict';

    var validator = require('../services/ValidatorService');

    exports.validateRequest = validateRequest;

    function validateRequest(req, res, next) {

        // post vars
        var title = validator.convertValueToString(req.body.title);
        var content = validator.convertValueToString(req.body.content);

        // title
        if (req.body.title && !validator.isAlpha(title) && !validator.isLength(title, 1, 100)) {
            throw new Error('Title too long.');
        }

        // content
        if (req.body.content && !validator.isAlpha(content) && !validator.isLength(content, 1, 1000)) {
            throw new Error('Post too long.');
        }

        next();

    }

})();