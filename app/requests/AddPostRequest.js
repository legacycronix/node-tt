(function () {

    'use strict';

    var validator = require('../services/ValidatorService');

    exports.validateRequest = validateRequest;

    function validateRequest(req, res, next) {

        // //console.log(req);
        //
        // // post vars
        // var title = validator.convertValueToString(req.body.title);
        // var content = validator.convertValueToString(req.body.content);
        // var user_id = validator.convertValueToString(req.body.user_id);
        //
        // // title
        // if (req.body.title && !validator.isAlpha(title) && !validator.isLength(title, 1, 100)) {
        //     throw new Error('Title too long.');
        // }
        //
        // // content
        // if (req.body.content && !validator.isAlpha(content) && !validator.isLength(content, 1, 1000)) {
        //     throw new Error('Post too long.');
        // }

        // launchpadId
        // if (!req.body.user_id || !validator.isInt(user_id)) {
        //     throw new Error('The user_id is requred!');
        // }

        return next();
    }

})();