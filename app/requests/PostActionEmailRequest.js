(function () {

    'use strict';

    var validator = require('../services/ValidatorService');

    exports.validateRequest = validateRequest;

    function validateRequest(req, res, next) {

        // post vars
        var name = validator.convertValueToString(req.body.name);
        var surname = validator.convertValueToString(req.body.surname);
        var email = validator.convertValueToString(req.body.email);
        var launchpadId = validator.convertValueToString(req.body.launchpadId);

        // name
        if (req.body.name && !validator.isAlpha(name) && !validator.isLength(name, 1, 100)) {
            throw new Error('The contact name is not the correct format.');
        }

        // surname
        if (req.body.surname && !validator.isAlpha(surname) && !validator.isLength(surname, 1, 100)) {
            throw new Error('The contact surname is not the correct format.');
        }

        // email
        if (!req.body.email || !validator.isEmail(email)) {
            throw new Error("The email is not the correct format.");
        }

        // launchpadId
        if (!req.body.launchpadId || !validator.isInt(launchpadId)) {
            throw new Error('The launchpad is requred.');
        }

        next();

    }

})();