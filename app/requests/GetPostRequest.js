(function () {

    'use strict';

    var validator = require('../services/ValidatorService');

    exports.validateRequest = validateRequest;

    function validateRequest(req, res, next) {

        // post vars
        var id = validator.convertValueToString(req.body.id);

        // id
        // if (!req.body.id || !validator.isInt(id)) {
        //     throw new Error('The id is requred.');
        // }

        next();

    }

})();