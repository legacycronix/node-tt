(function () {

    'use strict';

    var configApp = require('../../config/app')[process.env.NODE_ENV || 'test'];

    exports.getIndex = getIndex;
    exports.getHealth = getHealth;

    /**
     * API welcome response
     */
    function getIndex(req, res, next) {
        return res.json({
            message: 'Node-TT API V:' + configApp.VERSION
        });
    }

    /**
     * Is the API up and running
     */
    function getHealth(req, res, next) {
        return res.json({
            message: 'ONLINE'
        });
    }

})();