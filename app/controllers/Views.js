(function () {

    'use strict';

    var configApp = require('../../config/app')[process.env.NODE_ENV || 'test'];

    var dateService = require('../services/DateService');
    var db = require('../../database/models/index');

    exports.getTestPage = getTestPage;

    function getTestPage(req, res, next) {
        var users = [
            {firstName: 'A', lastName: 'B', email: 'a@b.com'},
            {firstName: 'C', lastName: 'D', email: 'c@d.com'}
        ];

        return res.render('../views/pages/test', {
            users: users
        });
    }

})();