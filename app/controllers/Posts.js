(function () {

    'use strict';

    var configApp = require('../../config/app')[process.env.NODE_ENV || 'test'];

    var dateService = require('../services/DateService');
    var db = require('../../database/models/index');

    exports.findPost = findPost;
    exports.getPosts = getPosts;
    exports.addPost = addPost;
    exports.editPost = editPost;
    exports.deletePost = deletePost;

    /**
     * Find a post
     *
     * @param req
     * @param res
     * @param next
     */
    function findPost(req, res, next) {
        var id = req.query.id;

        db.post.findById(id, {
            include: [
                {
                    model: db.user,
                    requred: false,
                    as: 'author'
                }
            ]
        }).then(function (foundPost) {
            return res.json({
                post: foundPost
            });
        }).catch(function (error) {
            next(error);
        });
    }

    /**
     * Get Posts by parameters
     *
     * @param req
     * @param res
     * @param next
     */
    function getPosts(req, res, next) {
        var title = req.query.title || '';
        var content = req.query.content || '';
        var user_id = req.query.user_id || 0;

        var offset = parseInt(req.query.offset) || 0;
        var limit = parseInt(req.query.limit) || 100;

        var where = {
            $or: [{
                content: {
                    $like: content
                },
                title: {
                    $like: title
                }
            }]
        };

        if (user_id) {
            where.user_id = user_id;
        }

        db.post.findAndCountAll({
            where: where,
            offset: offset,
            limit: limit,
            include: [
                {
                    model: db.user,
                    requred: false,
                    as: 'author'
                }
            ]
        }).then(function (result) {
            return res.json({
                count: result.count,
                posts: result.rows
            });
        }).catch(function (error) {
            next(error);
        });
    }

    /**
     * Add a post
     *
     * @param req
     * @param res
     * @param next
     */
    function addPost(req, res, next) {
        var title = req.body.title || '';
        var content = req.body.content || '';
        var user_id = req.body.user_id || 1;

        var now = dateService.now();

        db.post.create({
            title: title,
            content: content,
            user_id: user_id,
            createdAt: now,
            updatedAt: now
        }).then(function (newPost) {
            return res.json({
                post: newPost
            });
        }).catch(function (error) {
            next(error);
        });
    }

    /**
     * Edit post
     *
     * @param req
     * @param res
     * @param next
     */
    function editPost(req, res, next) {
        var id = req.body.id;

        var title = req.body.title || '';
        var content = req.body.content || '';

        var now = dateService.now();

        db.post.update(
            {
                title: title,
                content: content,
                updatedAt: now
            },
            {
                where: {
                    id: id
                }
            }
        ).then(function (updatedPost) {
            return res.json({
                post: updatedPost
            });
        }).catch(function (error) {
            return next(error);
        });

    }

    /**
     * Remove post
     *
     * @param req
     * @param res
     * @param next
     */
    function deletePost(req, res, next) {
        var id = req.body.id;

        var now = dateService.now();

        db.post.findOne({
            where: {
                id: id
            }
        }).then(function (foundPost) {
            return db.post.update(
                {
                    deletedAt: now
                },
                {
                    where: {
                        id: id
                    }
                }
            );
        }).then(function (updatedPost) {
            return res.json({
                post: updatedPost
            });
        }).catch(function (error) {
            return next(error);
        });
    }

})();