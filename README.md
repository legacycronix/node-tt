Node-TT

1) npm install

INSTALL:

2) INSTALL ALL MIGRATIONS

NODE_ENV=env node_modules/sequelize-cli/bin/sequelize db:migrate

3) INSTALL ALL SEEDS

NODE_ENV=env node_modules/sequelize-cli/bin/sequelize db:seed:all

// ---------------------------------------------------------------------------------------------------------------------

EXTRA:

UNDO ALL MIGRATIONS

NODE_ENV=env node_modules/sequelize-cli/bin/sequelize db:migrate:undo:all

UNDO ALL SEEDS

NODE_ENV=env node_modules/sequelize-cli/bin/sequelize db:seed:undo:all

RUN:

NODE_ENV=env node server.js

Where "env" your configuration in:

1) \config\app.json
1) \config\database.json

// =====================================================================================================================

WHAT YOU NEED TO DO:

1) Implement RESTFul for users, posts and comments.

We should have possibility to get (with search), add, edit and remove: users, posts and comments.

2) Add authentication for users by email/phone and password

Only authenticated user can see comments.

3) OPTIONAL: Implement simple page with forms where we can add users, posts and comments.


Other ideas and features will be big a plus.

For any questions/ideas contact me:

dmitry@cronix.ms