(function () {

    'use strict';

    module.exports = function (sequelize, DataTypes) {

        var bcrypt = require('bcryptjs');

        var post = sequelize.define('post', {
            title: {
                type: DataTypes.STRING
            },
            content: {
                type: DataTypes.STRING
            },
            userId: {
                type: DataTypes.INTEGER
            },
            createdAt: {
                type: DataTypes.DATE
            },
            updatedAt: {
                type: DataTypes.DATE
            },
            deletedAt: {
                type: DataTypes.DATE
            }
        }, {
            // actual name of table
            tableName: 'posts',
            // for soft deletes
            paranoid: true,
            // Associations
            classMethods: {
                associate: function (models) {
                    // author
                    post.belongsTo(models.user, {
                        as: 'author',
                        foreignKey: 'userId',
                        targetKey: 'id'
                    });
                }
            }
        });

        return post;

    };

}());
