(function () {

    'use strict';

    module.exports = function (sequelize, DataTypes) {

        var bcrypt = require('bcryptjs');

        var user = sequelize.define('user', {
            firstName: {
                type: DataTypes.STRING
            },
            lastName: {
                type: DataTypes.STRING
            },
            email: {
                type: DataTypes.STRING,
                unique: {
                    msg: 'The email address is already in use'
                },
                allowNull: false
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
                set: function (value) {
                    this.setDataValue('password', bcrypt.hashSync(value, 10));
                }
            },
            active: {
                type: DataTypes.BOOLEAN,
                defaultValue: 1
            },
            avatar: {
                type: DataTypes.STRING
            },
            about: {
                type: DataTypes.TEXT
            }
        }, {
            // actual name of table
            tableName: 'users',
            // for soft deletes
            paranoid: true,
            // Associations
            classMethods: {
                associate: function (models) {

                }
            }
        });

        return user;

    };

}());
