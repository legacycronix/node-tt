'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        var query = queryInterface.createTable(
            'posts', {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                title: {
                    type: Sequelize.STRING
                },
                content: {
                    type: Sequelize.STRING
                },
                userId: {
                    type: Sequelize.INTEGER,
                    allowNull: false
                },
                createdAt: {
                    type: Sequelize.DATE,
                    allowNull: false
                },
                updatedAt: {
                    type: Sequelize.DATE,
                    allowNull: false
                },
                deletedAt: {
                    type: Sequelize.DATE
                }
            }
        );

        return query;
    },

    down: function (queryInterface, Sequelize) {
        var query = queryInterface.dropTable('posts');

        return query;
    }
};
