(function () {

    'use strict';


    module.exports = {
        up: function (queryInterface, Sequelize) {
            var query = queryInterface.createTable(
                'users', {
                    id: {
                        type: Sequelize.INTEGER,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    firstName: {
                        type: Sequelize.STRING
                    },
                    lastName: {
                        type: Sequelize.STRING
                    },
                    email: {
                        type: Sequelize.STRING,
                        unique: true,
                        allowNull: false
                    },
                    password: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    active: {
                        type: Sequelize.BOOLEAN,
                        defaultValue: 0
                    },
                    avatar: {
                        type: Sequelize.STRING
                    },
                    about: {
                        type: Sequelize.TEXT
                    },
                    createdAt: {
                        type: Sequelize.DATE,
                        allowNull: false
                    },
                    updatedAt: {
                        type: Sequelize.DATE,
                        allowNull: false
                    },
                    deletedAt: {
                        type: Sequelize.DATE
                    }
                }
            );

            return query;

        },

        down: function (queryInterface, Sequelize) {
            var query = queryInterface.dropTable('users');

            return query;
        }
    };

})();
