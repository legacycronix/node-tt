(function () {

    'use strict';

    var env = process.env.NODE_ENV || 'production';
    var appConfig = require('../../config/app')[env];
    var dateService = require('../../app/services/DateService');

    var faker = require('faker');

    module.exports = {
        up: function (queryInterface, Sequelize) {
            if (appConfig.ENVIRONMENT === "production") {
                return;
            }

            var now = dateService.formatDateTime(dateService.now());

            var posts = [];

            for (var i = 0; i < 100; i++) {
                posts.push({
                    id: (i + 1),
                    title: faker.lorem.words(2),
                    content: faker.lorem.words(15),
                    userId: faker.random.number({
                        min: 1,
                        max: 100
                    }),
                    createdAt: now,
                    updatedAt: now
                });
            }

            return queryInterface.bulkInsert('posts', posts, {});
        },

        down: function (queryInterface, Sequelize) {
            return queryInterface.bulkDelete('posts', null, {});
        }
    };
})();