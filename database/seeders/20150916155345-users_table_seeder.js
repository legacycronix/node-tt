(function () {

    'use strict';

    var env = process.env.NODE_ENV || 'production';
    var appConfig = require('../../config/app')[env];
    var dateService = require('../../app/services/DateService');

    var faker = require('faker');

    var bcrypt = require('bcryptjs');

    module.exports = {
        up: function (queryInterface, Sequelize) {
            if (appConfig.ENVIRONMENT === "production") {
                return;
            }

            var now = dateService.formatDateTime(dateService.now());

            var users = [];

            for (var i = 0; i < 100; i++) {
                users.push({
                    id: (i + 1),
                    firstName: faker.name.firstName(),
                    lastName: faker.name.lastName(),
                    email: faker.internet.email(),
                    password: bcrypt.hashSync('12345', 10),
                    active: 1,
                    avatar: faker.image.avatar(),
                    about: faker.lorem.text(),
                    createdAt: now,
                    updatedAt: now
                });
            }

            return queryInterface.bulkInsert('users', users, {});
        },

        down: function (queryInterface, Sequelize) {
            return queryInterface.bulkDelete('users', null, {});
        }
    };
})();