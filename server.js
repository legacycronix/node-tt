(function () {

    'use strict';

    var configApp = require('./config/app')[process.env.NODE_ENV || 'test'];
    var configDb = require('./config/database')[process.env.NODE_ENV || 'test'];
    var serverService = require('./app/services/ServerService');


    var Sequelize = require('sequelize');
    var restful = require('sequelize-restful');
    var bodyParser = require('body-parser');
    var sequelize = new Sequelize(configDb.database, configDb.username, configDb.password, {
        logging: console.log,
        define: {
            timestamps: false
        }
    });

    var app = serverService.app;

    var compression = require('compression');
    var http = serverService.http;
    var morgan = require('morgan');
    var routes = require('./app/routes');

    /**
     * Set the view engine to ejs
     */
    app.set('view engine', 'ejs');

    /**
     * For development logging
     */
    app.use(morgan('dev'));

    /**
     * Compress all files
     */
    app.use(compression());

    /**
     * For reading POST variables
     */
    app.use(bodyParser.json());

    /** bodyParser.urlencoded(options)
     * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
     * and exposes the resulting object (containing the keys and values) on req.body
     */
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    /**
     * All routes
     */
    app.use('/', routes);

    console.log('process.env.PORT || configApp.PORT', process.env.NODE_ENV);

    /**
     * EB handles this on the servers
     */
    app.set('port', process.env.PORT || configApp.PORT);

    /**
     * Start the API Server
     */
    http.listen(app.get('port'), function () {
        console.log('API PORT: ' + app.get('port') + ' ENVIRONMENT: ' + configApp.ENVIRONMENT);
    });

})();